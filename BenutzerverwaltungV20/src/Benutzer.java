import java.util.Arrays;

class Benutzer {
	private String name;
    private char[] passwort;  // Verschlüsselt!	    
    private Benutzer next;
	private String anmeldestatus;
	private String letzterLogin;

    public Benutzer(String name, char[] pw){
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }
    
    public boolean hasPasswort(char[] cryptoPw){
        return Arrays.equals(this.passwort, cryptoPw);
    }
    
    public char[] getPasswort() {
    	return passwort;
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPasswort(char[] passwort) {
		this.passwort = passwort;
	}

	public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
    
    public String getAnmeldestatus() {
    	return anmeldestatus;
    }
    
    public void setAnmeldestatus(String status) {
    	anmeldestatus = status;
    }
    
    public String getLetzterLogin() {
    	return letzterLogin;
    }
    
    public void setLetzterLogin(String datum) {
    	letzterLogin = datum;
    }
}
