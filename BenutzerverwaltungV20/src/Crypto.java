class Crypto {
    private static int cryptoKey = 65500;
    private static String cryptoString = "thisIsTheEncryptionString";
    
    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }
    
    public static char[] encryptStronger(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoString.charAt(i) % cryptoString.length()) % 128);
        }
        return encrypted;
    }
    
    
    public static char[] decrypt(char[] s) {
    	char[] decrypted = new char[s.length];
    	
    	for (int i = 0; i < s.length; i++) {
    		//decrypted[i] = (char)((s[i] - cryptoKey) % 128);
    		decrypted[i] = (char)((s[i] + 128 - cryptoKey % 128) % 128);
    	}
    	return decrypted;
    }
}
