import java.util.Scanner;
import java.util.Arrays;
import java.text.SimpleDateFormat;  
import java.util.Date;  
// Vorbemerkung:
// Ich habe meine eigene Lösung von V1.0 in dieser Aufgabe weiterverwendet und soweit angepasst,
// dass ich die in der Aufgabenstellung gegebenen Methoden, Klassen etc. bei mir implementieren konnte.
//
// Aufgabe 1)
// Um das Adminpasswort innerhalb des Programms herauszufinden, wurde eine weitere
// "geheime" Menüoption hinzugefügt, welche per 1337 aufrufbar ist. Diese führt die Methode
// decryptPasswort aus und kann somit das Passwort eines jeden Nutzers im Klartext anzeigen. 
// Die Methode um das Passwort zu entschlüsseln, wurde der Crypto-Klasse hinuzgefügt.
// Das Admin-Passwort lautet Hallo123. Im echten Produktionsumfeld sollte eine solche Option selbstverständlich
// nicht existieren.
//
// Aufgabe 3)
// Für die Lösung dieser Aufgabe habe ich die Vigenère-Methode angewendet. Das Schlüsselwort habe ich
// statisch in der Crypto-Methode definiert.


class BenutzerverwaltungV20 {
    static boolean programmWirdAusgeführt = true;

	 public static void start(){
	        BenutzerListe benutzerListe = new BenutzerListe();
	        Scanner tastatur = new Scanner(System.in);

	        benutzerListe.insert(new Benutzer("Paula", Crypto.encryptStronger("paula".toCharArray())));
	        benutzerListe.insert(new Benutzer("Adam37", Crypto.encryptStronger("adam37".toCharArray())));
	        benutzerListe.insert(new Benutzer("Darko", Crypto.encryptStronger("darko".toCharArray())));
	        benutzerListe.insert(new Benutzer("Admin", new char[]{36, 61, 72, 72, 75, 13, 14, 15}));
	        
	        boolean keineGültigeAuswahlGetroffen;
	        
	        do {
		        System.out.println("Was möchten Sie tun?\n");
		        System.out.println("Anmelden (1)");
		        System.out.println("Registrieren (2)\n");
		        System.out.print("Ihre Auswahl: ");
		        
		        int auswahl = tastatur.nextInt();
		        
		        if (auswahl == 1) {
		        	anmelden(tastatur, benutzerListe);
		        	keineGültigeAuswahlGetroffen = false;
		        } else if (auswahl == 2) {
		        	registrieren(tastatur, benutzerListe);
		        	keineGültigeAuswahlGetroffen = false;
		        } else if (auswahl == 1337) {
		        	decryptPasswort(tastatur, benutzerListe);
		        	keineGültigeAuswahlGetroffen = false;
		        } else {
		        	System.out.println("Bitte treffen Sie eine gültige Auswahl.");
		        	keineGültigeAuswahlGetroffen = true;
		        }
	        	
	        } while (keineGültigeAuswahlGetroffen || programmWirdAusgeführt);
	    }
	 
	 public static boolean authenticate(String name, char[] cryptoPw, BenutzerListe benutzerListe) { 
		 Benutzer b = benutzerListe.sucheBenutzer(name);
		 if(b != null) {
			 if(b.hasPasswort(cryptoPw))
			 {
				 return true;
			 }
		 }
	         return false;
     }
	 
	 public static void anmelden(Scanner tastatur, BenutzerListe benutzerListe) {
		 String name;
		 char[] passwort;
		 int versuche = 0;
		
		 
		 while (versuche < 3)
		 {
			 System.out.print("Bitte geben Sie Ihren Benutzernamen ein:");
			 name = tastatur.next();

			 System.out.print("Bitte geben Sie Ihr Passwort ein:");
			 String inputPasswort = tastatur.next();
			 passwort = Crypto.encryptStronger(inputPasswort.toCharArray());
			 
			 Benutzer benutzer = benutzerListe.sucheBenutzer(name);
			 
			 if(!authenticate(name, passwort, benutzerListe)) {
				 System.out.println("Benutzername oder Passwort falsch. Bitte versuchen Sie es noch einmal.");
				 name = null;
				 passwort = null;
				 versuche++;
			 } else {
				 System.out.println("***Anmeldung erfolgreich!***");
				 
				 Date date = new Date();
				 SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");  
				 benutzer.setLetzterLogin(formatter.format(date));
				 
				 benutzer.setAnmeldestatus("online");
				 System.out.println("Programm beenden? [j/n]");
				 programmWirdAusgeführt = !tastatur.next().equals("j");
				 if (!programmWirdAusgeführt) {
					 System.out.println("Bis bald!");
				 }
				 return;
			 }
		 }
		 System.out.println("Anmeldung fehlgeschlagen.");
		 return;
	 }
	 
	 public static void registrieren(Scanner tastatur, BenutzerListe benutzerListe) {
		 String name;
		 char[] passwort;
		 char[] passwortBestätigung;
		 boolean nutzerBereitsVorhanden = false;
		 
		 do {
			 System.out.print("Bitte geben Sie Ihren gewünschten Benutzernamen ein:");
			 
			 name = tastatur.next();
			 
			 if (benutzerListe.sucheBenutzer(name) != null) {
				 System.out.println("Dieser Nutzer existiert bereits!");
				 nutzerBereitsVorhanden = true;
			 } else {
				 boolean passwörterSindUngleich = false;
				 nutzerBereitsVorhanden = false;
				 
				 do {
					 System.out.print("Bitte geben Sie Ihr gewünschtes Passwort ein:");
					 passwort = tastatur.next().toCharArray();
					 System.out.print("Bitte bestätigen Sie Ihr Passwort:");
					 passwortBestätigung = tastatur.next().toCharArray();
					 
					 if (Arrays.equals(passwort,passwortBestätigung)) {
						 benutzerListe.insert(new Benutzer(name, Crypto.encryptStronger(passwort)));
						 System.out.println("Sie wurden erfolgreich registriert, " + name + ".");
						 passwörterSindUngleich = false;
						 
						 System.out.println("Programm beenden? [j/n]");
						 programmWirdAusgeführt = !tastatur.next().equals("j");
						 if (!programmWirdAusgeführt) {
							 System.out.println("Bis bald!");
						 }
					 } else {
						 System.out.println("Die Passwörter stimmen nicht überein.\n");
						 passwörterSindUngleich = true;
					 }
				 } while (passwörterSindUngleich);
			 }
			 
		 } while (nutzerBereitsVorhanden);
	 }
	 
	 public static void decryptPasswort(Scanner tastatur, BenutzerListe benutzerListe) {
		 System.out.println("Das Passwort von welchem Nutzer möchten Sie anzeigen?");
		 String benutzerName = tastatur.next();
		 
		 if (benutzerListe.sucheBenutzer(benutzerName) != null) {
			 Benutzer gewaehlterBenutzer = benutzerListe.sucheBenutzer(benutzerName);
			 System.out.println("Der Benutzer '" + benutzerName + "' wurde gefunden.");

			 char[] passwort = gewaehlterBenutzer.getPasswort();
			 String stringPasswort = new String(passwort);
			 String entschluesseltesPasswort = new String(Crypto.decrypt(passwort));
			 System.out.println("Passwort verschlüssel: " + stringPasswort);
			 System.out.println("Passwort entschlüsselt: " + entschluesseltesPasswort);

		 } else {
			 System.out.println("Der Benutzer '" + benutzerName + "' wurde nicht gefunden.\nAbbruch.");

		 }

	 }
}
