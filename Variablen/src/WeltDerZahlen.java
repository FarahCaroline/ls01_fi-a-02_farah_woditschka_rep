/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Farah Woditschka >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  9;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 300000000000L;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 10523;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
   int gewichtKilogramm = 190000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
   int flaecheGroessteLand = 17100000;
    
    // Wie groß ist das kleinste Land der Erde?
    
   double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Einwohner Berlins: " + bewohnerBerlin);
    
    System.out.println("Anzahl der Tage, die ich alt bin: " + alterTage);
    
    System.out.println("Gewicht des schwersten Tiers der Welt: " + gewichtKilogramm + " kg");
    
    System.out.println("Fläche des größten Lands der Welt (Russland): " + flaecheGroessteLand + " qm");
    
    System.out.println("Fläche des kleinsten Lands der Welt (Vatikanstaat): " + flaecheKleinsteLand + " qm");
    
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

