
public class Ausgabe {

	public static void main(String[] args) {
		System.out.printf("%-12s%s%10s", "Fahrenheit", "|", "Celcius");
		System.out.printf("\n%s%s%s%s%s%s%s%s%s%s%s%s", "- ", "- ", "- ", "- ", "- ","- ", "- ","- ","- ","- ","- ","- ", "- ");
		System.out.printf("\n%-12d%s%10.2f", -20, "|", -28.9);
		System.out.printf("\n%-12d%s%10.2f", -10, "|", -23.33);
		System.out.printf("\n%+-12d%s%10.2f", 0, "|", -6.67);
		System.out.printf("\n%+-12d%s%10.2f", 20, "|", -28.9);
		System.out.printf("\n%+-12d%s%10.2f", 30, "|", -1.11);
		
	}
}
