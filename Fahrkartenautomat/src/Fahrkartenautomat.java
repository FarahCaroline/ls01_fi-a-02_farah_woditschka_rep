﻿import java.util.Scanner;

// VON FARAH WODITSCHKA

// Fahrkartenautomat 07 - AUFGABE 1
/*Schreiben Sie das Programm Fahrkartenautomat (die Methode fahrkartenbestellungErfassen) um:
Benutzen Sie für die Verwaltung der Fahrkartenbezeichnung und der Fahrkartenpreise jeweils ein Array.
Welche Vorteile hat man durch diesen Schritt?

Der Vorteil besteht vor allem darin, dass man für die verschiedenen Auswahlmöglichkeiten weder eine if-Abfrage, 
noch eine Switch-Case-Funktion benötigt, sondern lediglich sich den Index der getroffenen Auswahl über die Auswahlnummer generieren muss.
Außerdem können so sehr einfach neue Ticketmöglichkeiten ergänzt oder gelöscht werden.

AUFGABE 3
Dadurch, dass nun mit Arrays gearbeitet wird, konnten sehr einfach weitere Tickets hinzugefügt werden. Außerdem konnten if-Abfragen und
switch-case-Funktionen durch eine for-Schleife ersetzt und so der Code übersichtlicher und leichter lesbar gestaltet werden.
Der größte Nachteil dieser Umstellung besteht darin, dass man unbedingt darauf achten muss, dass die Ticketbeschreibungen und die dazugehörigen
Preise jeweils an den gleichen Stellen in ihren Arrays stehen.

*/
class Fahrkartenautomat {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		double rückgabebetrag;
		double zuZahlenderBetrag;
		char abschalten = 'j';

		while (abschalten == 'j') {

			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			// Geldeinwurf
			// -----------

			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------
			rueckgeldAusgeben(rückgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");

			System.out.println("Nächster Kunde? [j/n]");
			abschalten = tastatur.next().charAt(0);
		}
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double ticketpreis;
		int anzahlDerFahrkarten;
		int auswahlnummer;

		String[] fahrkartenBezeichnung = {
				"Einzelfahrschein Berlin AB", 
				"Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC",
				"Kurzstrecke",
				"Tageskarte Berlin AB",
				"Tageskarte Berlin BC",
				"Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB",
				"Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC"
				};
	
		
		double[] fahrkartenPreise = {
				2.90,
				3.30,
				3.60,
				1.90,
				8.60,
				9.00,
				9.60,
				23.50,
				24.30,
				24.90
				};
		
		
		System.out.println("Fahrkartenbestellung");
		for (int i = 0; i < 20; i++) {
			System.out.print("=");
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

		System.out.println("Willkommen!\nBitte wählen Sie Ihre Wunschfahrkarte aus: \n");
		for (int i = 0; i < fahrkartenBezeichnung.length; i++) {
			int auswahl = i+1;
			System.out.printf("%-35s%s%5.2f%s%5s\n",fahrkartenBezeichnung[i], " [", fahrkartenPreise[i], " EUR]", "(" + auswahl + ")");
		}
		
		System.out.print("\nIhre Wahl: ");
		
		auswahlnummer = tastatur.nextInt();

		while (auswahlnummer < 0 || auswahlnummer > 10) {
			System.out.println("Fehler! Ungültige Auswahlnummer.");
			System.out.print("\nIhre Wahl: ");
			auswahlnummer = tastatur.nextInt();
		}
		
		
		System.out.print("Sie haben gewählt: " + fahrkartenBezeichnung[auswahlnummer -1] + "\nAnzahl der Fahrkarten: ");
		anzahlDerFahrkarten = tastatur.nextInt();

		if (anzahlDerFahrkarten > 10) {
			System.out.println("Fehler! Es können nicht mehr als 10 Tickets gekauft werden. Anzahl der Tickets: 1");
			anzahlDerFahrkarten = 1;
		} else if (anzahlDerFahrkarten < 1) {
			System.out.println("Fehler! Mindestens 1 Ticket muss bestellt werden. Anzahl der Tickets: 1");
			anzahlDerFahrkarten = 1;
		}
		
		double fahrkartenAuswahlPreis = fahrkartenPreise[auswahlnummer -1];

		return (double) anzahlDerFahrkarten*fahrkartenAuswahlPreis;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("%s%.2f%s\n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
					" EURO");
			System.out.printf("%s%.2f%s%.2f%s", "Eingabe (mind. ", 0.05, " Euro, höchstens ", 2.0, " Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return (eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100;
	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.printf("%s%.2f%s\n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag / 100, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:\n");

			String format = "%.2f%s\n";

			while (rückgabebetrag >= 200) // 2 EURO-Münzen
			{
				System.out.printf(format, 2.0, " EURO");
				rückgabebetrag -= 200;
			}
			while (rückgabebetrag >= 100) // 1 EURO-Münzen
			{
				System.out.printf(format, 1.0, " EURO");
				rückgabebetrag -= 100;
			}
			while (rückgabebetrag >= 50) // 50 CENT-Münzen
			{
				System.out.printf(format, 0.50, " EURO");
				rückgabebetrag -= 50;
			}
			while (rückgabebetrag >= 20) // 20 CENT-Münzen
			{
				System.out.printf(format, 0.20, " EURO");
				rückgabebetrag -= 20;
			}
			while (rückgabebetrag >= 10) // 10 CENT-Münzen
			{
				System.out.printf(format, 0.10, " EURO");
				rückgabebetrag -= 10;
			}
			while (rückgabebetrag >= 5)// 5 CENT-Münzen
			{
				System.out.printf(format, 0.05, " EURO");
				rückgabebetrag -= 5;
			}
		}

	}

}