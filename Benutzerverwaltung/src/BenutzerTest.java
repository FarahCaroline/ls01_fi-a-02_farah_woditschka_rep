
public class BenutzerTest {
	
	public static void main(String[] args) {
		Benutzer user1 = new Benutzer("Alan Turing");
		
		user1.setEmail("turing@enigma.co.uk");
		user1.setPassword("codeCracker123");
		user1.setBenutzerstatus("deaktiviert");
		user1.setAnmeldestatus("offline");
		user1.setLetzterLogin("7. Juni 1954");
		
		System.out.println("Name: " + user1.getName());
		System.out.println("E-Mail: " + user1.getEmail());
		System.out.println("Passwort: " + user1.getPassword());
		System.out.println("Benutzerstatus" + user1.getBenutzerstatus());
		System.out.println("Anmeldestatus: " + user1.getAnmeldestatus());
		System.out.println("Letzter Login: " + user1.getLetzterLogin());
		
		System.out.println("\n//////////////\n");
		
		Benutzer user2 = new Benutzer("Ada Lovelace");

		user2.setEmail("lovelace@analytical-engine.co.uk");
		user2.setPassword("iLoveMaths");
		user2.setBenutzerstatus("deaktiviert");
		user2.setAnmeldestatus("offline");
		user2.setLetzterLogin("27. November 1852");
		
		System.out.println("Name: " + user2.getName());
		System.out.println("E-Mail: " + user2.getEmail());
		System.out.println("Passwort: " + user2.getPassword());
		System.out.println("Benutzerstatus: " + user2.getBenutzerstatus());
		System.out.println("Anmeldestatus: " + user2.getAnmeldestatus());
		System.out.println("Letzter Login: " + user2.getLetzterLogin());
				
	}
}
