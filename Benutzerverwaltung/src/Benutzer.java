
public class Benutzer {

	private String name;
	private String email;
	private String password;
	private String benutzerstatus;
	private String anmeldestatus;
	private String letzterLogin;


	public Benutzer(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBenutzerstatus() {
		return benutzerstatus;
	}

	public void setBenutzerstatus(String benutzerstatus) {
		this.benutzerstatus = benutzerstatus;
	}

	public String getAnmeldestatus() {
		return anmeldestatus;
	}

	public void setAnmeldestatus(String anmeldestatus) {
		this.anmeldestatus = anmeldestatus;
	}

	public String getLetzterLogin() {
		return letzterLogin;
	}

	public void setLetzterLogin(String letzterLogin) {
		this.letzterLogin = letzterLogin;
	}

}
