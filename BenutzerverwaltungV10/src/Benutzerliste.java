
class BenutzerListe {
	private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public String select(String name){
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public void delete(String name){
    	Benutzer zuLöschenderBenutzer = first;
        
        while (zuLöschenderBenutzer != null) {
       	 if(zuLöschenderBenutzer.getName().equals(name)) {
       		 if (zuLöschenderBenutzer == first) {
       			 first = first.getNext();
       			 zuLöschenderBenutzer.setNext(null);
       		 } else {
       			 Benutzer vorherigerBenutzer = first;
       			 
       			 while (vorherigerBenutzer != null && vorherigerBenutzer.getNext() != null) {
           			 if (vorherigerBenutzer.getNext().getName().equals(name)) {
           				 if (zuLöschenderBenutzer.getNext() != null) {
           					vorherigerBenutzer.setNext(zuLöschenderBenutzer.getNext());
           					zuLöschenderBenutzer.setNext(null);
           				 } else {
           					vorherigerBenutzer.setNext(null);
           				 }
           			 }
           			vorherigerBenutzer = vorherigerBenutzer.getNext();
       			 }
       		 }
       	 }
       	zuLöschenderBenutzer = zuLöschenderBenutzer.getNext();
        }
    }
    
    public Benutzer sucheBenutzer(String name) {
    	Benutzer b = first;
    	while (b != null) {
    		if(b.hasName(name)) {
    			return b;
    		}
    		b = b.getNext();
    	}
    	return null;
    }
}
