import java.util.Scanner;
import java.text.SimpleDateFormat;  
import java.util.Date;  

class BenutzerverwaltungV10 {
	
	 public static void start(){
	        BenutzerListe benutzerListe = new BenutzerListe();
	        Scanner tastatur = new Scanner(System.in);

	        benutzerListe.insert(new Benutzer("Paula", "paula"));
	        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
	        benutzerListe.insert(new Benutzer("Darko", "darko"));
	        
	        boolean keineGültigeAuswahlGetroffen;
	        
	        do {
		        System.out.println("Was möchten Sie tun?\n");
		        System.out.println("Anmelden (1)");
		        System.out.println("Registrieren (2)\n");
		        System.out.print("Ihre Auswahl: ");
		        
		        int auswahl = tastatur.nextInt();
		        
		        if (auswahl == 1) {
		        	anmelden(tastatur, benutzerListe);
		        	keineGültigeAuswahlGetroffen = false;
		        } else if (auswahl == 2) {
		        	registrieren(tastatur, benutzerListe);
		        	keineGültigeAuswahlGetroffen = false;
		        } else {
		        	System.out.println("Bitte treffen Sie eine gültige Auswahl.");
		        	keineGültigeAuswahlGetroffen = true;
		        }
	        	
	        } while (keineGültigeAuswahlGetroffen);
	    }
	 
	 public static void anmelden(Scanner tastatur, BenutzerListe benutzerListe) {
		 String name;
		 String passwort;
		 int versuche = 0;
		
		 
		 while (versuche < 3)
		 {
			 System.out.print("Bitte geben Sie Ihren Benutzernamen ein:");
			 name = tastatur.next();

			 System.out.print("Bitte geben Sie Ihr Passwort ein:");
			 passwort = tastatur.next();
			 
			 Benutzer benutzer = benutzerListe.sucheBenutzer(name);
			 
			 if(benutzer == null || !passwort.equals(benutzer.getPasswort())) {
				 System.out.println("Benutzername oder Passwort falsch. Bitte versuchen Sie es noch einmal.");
				 name = null;
				 passwort = null;
				 versuche++;
			 } else {
				 System.out.print("***Anmeldung erfolgreich!***");
				 
				 Date date = new Date();
				 SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");  
				 benutzer.setLetzterLogin(formatter.format(date));
				 
				 benutzer.setAnmeldestatus("online");
				 return;
			 }
		 }
		 System.out.println("Anmeldung fehlgeschlagen.");
		 return;
	 }
	 
	 public static void registrieren(Scanner tastatur, BenutzerListe benutzerListe) {
		 String name;
		 String passwort;
		 String passwortBestätigung;
		 boolean nutzerBereitsVorhanden = false;
		 
		 do {
			 System.out.print("Bitte geben Sie Ihren gewünschten Benutzernamen ein:");
			 
			 name = tastatur.next();
			 
			 if (benutzerListe.sucheBenutzer(name) != null) {
				 System.out.println("Dieser Nutzer existiert bereits!");
				 nutzerBereitsVorhanden = true;
			 } else {
				 boolean passwörterSindUngleich = false;
				 nutzerBereitsVorhanden = false;
				 
				 do {
					 System.out.print("Bitte geben Sie Ihr gewünschtes Passwort ein:");
					 passwort = tastatur.next();
					 System.out.print("Bitte bestätigen Sie Ihr Passwort:");
					 passwortBestätigung = tastatur.next();
					 
					 if (passwort.equals(passwortBestätigung)) {
						 benutzerListe.insert(new Benutzer(name, passwort));
						 System.out.println("Sie wurden erfolgreich registriert, " + name + ".");
						 passwörterSindUngleich = false;
					 } else {
						 System.out.println("Die Passwörter stimmen nicht überein.\n");
						 passwörterSindUngleich = true;
					 }
				 } while (passwörterSindUngleich);
			 }
			 
		 } while (nutzerBereitsVorhanden);
	 }
}
