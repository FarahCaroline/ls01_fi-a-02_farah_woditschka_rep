
public class Ausgabe {

	public static void main(String[] args) {
		System.out.printf("%-5s%-2s%19s%4d\n", "0!", "=", "=", 1 );
		System.out.printf("%-5s%-2s%d%18s%4d", "1!", "=", 1, "=", 1 );
		System.out.printf("\n%-5s%-2s%-2d%-2s%-14d%s%4d", "2!", "=", 1, "*", 2, "=", 2 );
		System.out.printf("\n%-5s%-2s%-2d%-2s%-2d%-2s%-10d%s%4d", "3!", "=", 1, "*", 2, "*", 3, "=", 6 );
		System.out.printf("\n%-5s%-2s%-2d%-2s%-2d%-2s%-2d%-2s%-6d%s%4d", "4!", "=", 1, "*", 2, "*", 3, "*", 4, "=", 24 );
		System.out.printf("\n%-5s%-2s%-2d%-2s%-2d%-2s%-2d%-2s%-2d%-2s%-2d%s%4d", "5!", "=", 1, "*", 2, "*", 3, "*", 4, "*", 5, "=", 120 );

	}

}
